function compareByValue(x,y)
        x = string(x[1:end-1])
        y = string(y[1:end-1])
        v = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Q", "J", "K", "A"]
        for i in 1:length(v)
                if x == v[i]
                        x = i
                end
                if y == v[i]
                        y = i
                end
        end
        if x < y
                return true
        else
                return false
        end
end

function troca(v, i, j)
  aux = v[i]
  v[i] = v[j]
  v[j] = aux
end

function insercao(v)
        tam = length(v)
        for i in 2:tam
                j = i
                while j > 1
                        if compareByValue(v[j], v[j - 1])
                                troca(v, j, j - 1)
                        else
                                break
                        end
                j = j - 1
                end
        end
        return v
end

function compareByValueAndSuit(x,y)
        x1 = string(x[end:end])
        y1 = string(y[end:end])
        x = string(x[1:end-1])
        y = string(y[1:end-1])
        v = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Q", "J", "K", "A"]
        w = ["♦", "♠" , "♥", "♣"]
        for i in 1:length(v)
                if x == v[i]
                        x = i
                end
                if y == v[i]
                        y = i
                end
        end
        for j in 1:length(w)
                if x1 == w[j]
                        x1 = j
                end
                if y1 == w[j]
                        y1 = j
                end
        end
        if x1 < y1
                return true
        elseif x < y
                return true
        else
                return false
        end
end

functionn insercao(v)
        tam = length(v)
        for i in 2:tam
                j = i
                while j > 1
                        if compareByValueAndSuit(v[j], v[j - 1])
                                troca(v, j, j - 1)
                        else
                                break
                        end
                j = j - 1
                end
        end
        return v
end

using Test
function testes()
	@test compareByValue("2♠", "A♠")
	@test !compareByValue("K♥", "10♥")
	@test compareByValueAndSuit("2♠", "A♠")
	@test !compareByValueAndSuit("K♥", "10♥")
	@test compareByValueAndSuit("10♠", "10♥")
	@test compareByValueAndSuit("A♠", "2♥")
	println("Fim dos testes")
end

# testes()
